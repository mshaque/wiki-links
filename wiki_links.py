import requests
from bs4 import BeautifulSoup
import re

def read_file_to_memory(file_address):
    with open(file_address, 'r') as content_file:
        return content_file.read()


def get_lines(file_address):
    file_content = read_file_to_memory(file_address)
    line_list = file_content.split("\n")
    return line_list





if __name__ == "__main__":
    mapper = re.compile(r'(>)([^>]*)(<)')
    dump = []
    lines = get_lines("tmp_add.txt")
    final_line_list = []
    for line in lines:
        if "<li><a href=" not in line:
            continue

        elements = line.split("\"")
        address = "https://en.m.wikipedia.org" + str(elements[1])
        response = requests.get(address)
        soup = BeautifulSoup(response.content,"lxml")
        tbl_item = soup.findAll("table", {"class": "infobox vcard"})
        if len(tbl_item) <1:
            print(address)
            continue

        tbl_item = soup.findAll("table", {"class": "infobox vcard"})[0]

        tbl_lines = str(tbl_item).split("\n")
        tmp_org = []
        link_list = []
        for tbl_line in tbl_lines:
            if "href=" in tbl_line:
                tmp_link = tbl_line.split("\"")
                for link_item in tmp_link:
                    if "htt" in link_item:
                        link_list.append(link_item)


            matcher = mapper.findall(tbl_line)
            for match in matcher:
                tmpStr = str(match[1])
                if len(tmpStr) >3:
                    tmp_org.append(tmpStr)

        # seperating the fields using 'pipe'
        printStr = "|" + address  + "|" +  tmp_org[0] + "|" + ",".join(link_list) + "|" + "!".join(tmp_org)

        dump.append(printStr)




    output = "\n".join(dump)

    # copy paste the printout from console to excel
    print(output)



